Role Name
=========

ansible playbook role for deploy jaeger distributed tracing system

Requirements
------------



Role Variables
--------------

jager_es_urls: url to es storage backend

Dependencies
------------

it required consul to do service register
it required elasticsearch to be installed and give a es url

Example Playbook
----------------


    - hosts: servers
      vars:
        group_names:
          - jaeger_query
          - jaeger_agent
          - jaeger_collector
      roles:
         - { role: jaeger, jaeger_es_urls: '127.0.0.1:9200'}

License
-------

BSD

Author Information
------------------

name: hackrole
email: hack.role@gmail.com
